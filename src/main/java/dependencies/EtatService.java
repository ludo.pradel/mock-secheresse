package dependencies;

public interface EtatService {
    /**
     * Permet d'alerter l'état du manque critique d'eau sur votre commune
     */
    public void sendAlerteEau();

}
